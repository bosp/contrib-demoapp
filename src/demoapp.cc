/* Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "demoexc.h"

#define EXC_BASENAME 	"D"

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "demoapp"

namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;
typedef std::shared_ptr<DemoWorkload> dwEXCPtr_t;

/**
 * The decription of each testapp parameters
 */
po::options_description opts_desc("DemoApp Configuration Options");

/**
 * The map of all testapp parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief User input choice
 *
 * '+' Push up the AWMs lower bound
 * '-' Push down the AWMs upper bound
 * 'g' Assert a goal NAP
 * 'q' = quit
 */
char user_choice;

/**
 * @brief Goal Gap percentage (NAP)
 */
unsigned short ggap;


void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "Barbeque RTLib DemoApp\n";
		std::cout << "Copyright (C) 2012 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

/**
 * @brief Register the required EXCs and enable them
 *
 * @param name the recipe to use
 */
dwEXCPtr_t SetupEXC(uint8_t exc_id,
		const char *recipe, uint8_t nthreads,
		uint8_t	max_awm) {
	dwEXCPtr_t pexc;
	char exc_name[10];
	const char * prio_bg;
	char prio_info[3];

	// Setup EXC name and recipe name
	prio_bg = strchr(recipe, '_');
	if (prio_bg)
		snprintf(prio_info, 3, "%s", prio_bg+1);
	else
		snprintf(prio_info, 1, "--");
	snprintf(exc_name, 10, EXC_BASENAME "%02dP%s", exc_id, prio_info);

	// Register the EXC
	pexc = dwEXCPtr_t(new DemoWorkload(exc_name, recipe, rtlib,
				RUN_CYCLE_MS, nthreads, max_awm));
	if (!pexc->isRegistered())
		return dwEXCPtr_t();

	// Starting the control thread for the EXC
	assert(pexc);
	pexc->Start();

	return pexc;
}

/**
 * @brief Print the option menu
 */
void ShowMenu() {
	fprintf(stdout, FN(" ____________________________\n"));
	fprintf(stdout, FN("|                            |\n"));
	fprintf(stdout, FN("|                Option Menu |\n"));
	fprintf(stdout, FN("|____________________________|\n"));
	fprintf(stdout, FN("|                            |\n"));
	fprintf(stdout, FN("| - : Decrease highest AWM   |\n"));
	fprintf(stdout, FN("| + : Increase highest AWM   |\n"));
	fprintf(stdout, FN("| n : Set goal NAP (%d%c)     |\n"), ggap, '%');
	fprintf(stdout, FN("| q : Quit                   |\n"));
	fprintf(stdout, FN("|____________________________|\n"));
}

/**
 * @brief User interface
 */
RTLIB_ExitCode_t RunEXC(dwEXCPtr_t pexc) {
	while (user_choice != 'q') {
		// Option Menu
		ShowMenu();

		// Capture the user choice
		user_choice = getc(stdin);
		switch (user_choice) {
		// Move up the lower bound
		case '+':
			pexc->IncUpperAwmID();
			break;
		// Shrink down the low bound
		case '-':
			pexc->DecUpperAwmID();
			break;
		// Shrink down the low bound
		case 'n':
			pexc->IncUpperAwmID();
			pexc->SetGoalGap(70);
			break;
		// Quit
		case 'q':
			pexc->SetFinished();
			break;
		}
	}

	return RTLIB_OK;
}

/**
 * @brief Unregister all the EXCs
 */
RTLIB_ExitCode_t DestroyEXC(dwEXCPtr_t pexc) {
	pexc->Terminate();

	return RTLIB_OK;
}

int main(int argc, char *argv[]) {
	dwEXCPtr_t pexc;
	unsigned short exc_id;
	unsigned short num_threads;
	unsigned short max_awm;
	std::string recipe;

	opts_desc.add_options()
		("help,h", "print this help message")
		("recipe,r", po::value<std::string>(&recipe)->
			default_value("BbqDemoi7_0B"),
			"Recipe name")
		("exc,e", po::value<unsigned short>(&exc_id)->
			default_value(1),
			"ID number (of the EXC)")
		("threads,t", po::value<unsigned short>(&num_threads)->
			default_value(DEFAULT_MAX_NUM_THREADS),
			"Max number of threads")
		("nap,n", po::value<unsigned short>(&ggap)->
			default_value(50),
			"Normalized Actual Penalty for NAP assertion")
		("awm,w", po::value<unsigned short>(&max_awm)->
			default_value(2),
			"Maximum AWM ID")
	;

	ParseCommandLine(argc, argv);

	// Welcome screen
	fprintf(stdout, FI(".:: Barbeque RTLib DemoApp (ver. %s) ::.\n"),
			g_git_version);
	fprintf(stdout, FI("Built: " __DATE__  " " __TIME__ "\n"));

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);

	// Configuring required Execution Contexts
	pexc = SetupEXC(exc_id, recipe.c_str(), num_threads, max_awm);
	if (!pexc) {
		fprintf(stderr, FE("Error while launching DemoWorkloadEXC..\n"));
		return EXIT_FAILURE;
	}

	// Wait for workload processing to terminate
	RunEXC(pexc);

	pexc->WaitCompletion();
	fprintf(stderr, FI("===== RTLibDemoApp DONE! =====\n"));
	return EXIT_SUCCESS;
}

