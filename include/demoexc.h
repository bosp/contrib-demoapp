/* Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTLIB_DEMO_EXC_H_
#define RTLIB_DEMO_EXC_H_

#include "bbque/cpp11/condition_variable.h"
#include "bbque/cpp11/mutex.h"
#include "bbque/cpp11/thread.h"
#include <vector>

#include <bbque/bbque_exc.h>
#include <bbque/utils/utility.h>

#define DEFAULT_MAX_NUM_THREADS 	32
#define RUN_CYCLE_MS            	200

using bbque::rtlib::BbqueEXC;


class DemoWorkload : public BbqueEXC {

public:

	DemoWorkload(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			uint32_t cycle_ms,
			uint8_t max_nthds,
			uint8_t max_awm);

	~DemoWorkload();

	void IncUpperAwmID();
	void DecUpperAwmID();
	void SetFinished();

private:

	std::mutex start_mtx;
	std::mutex run_mtx;
	std::condition_variable start_cv;
	std::condition_variable run_cv;

	std::mutex tds_enable_mtx[DEFAULT_MAX_NUM_THREADS];
	std::mutex tds_done_mtx[DEFAULT_MAX_NUM_THREADS];

	std::condition_variable tds_enable_cv[DEFAULT_MAX_NUM_THREADS];
	std::condition_variable tds_done_cv[DEFAULT_MAX_NUM_THREADS];

	std::vector<std::thread> td_pool;
	bool tds_done[DEFAULT_MAX_NUM_THREADS];

	/** Length of run cycle */
	uint32_t cycle_ms;

	/** Maximum number of threads */
	uint8_t max_num_tds;

	/** Highest AWM ID */
	uint8_t max_awm_id;


	/** Active threads*/
	uint8_t high_active_thread_id;

	/** Run cycles counter */
	uint32_t run_count;

	/** Synchronize the starting of the threads */
	uint8_t starting_threads;


	/** Current AWM upper bound ID */
	uint8_t awm_upper_id;

	/** True if the upper bound has changed */
	bool upper_changed;


	/** If true, the application must exit */
	bool finished;


	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onSuspend();
	RTLIB_ExitCode_t onRelease();

	/** Thread work function */
	void WorkFunction(uint8_t id, double run_time);
};

#endif // RTLIB_DEMO_EXC_H_

